## LED GUIDE
### HOW TO INTERPRET THE LEDs

#### Introduction
The submodule implements 3 status leds that give a visual representation of the state of the powers supply. All LEDs function simultaneously with the same color and brightness.

##### 1. LEDs are OFF

When the LEDs are off, it means that the power supply is not supplying power to the HAT at that moment. 

##### 2. LEDS are ON
When the LEDs are on, the PSU is supplying power to keep the Pi on
