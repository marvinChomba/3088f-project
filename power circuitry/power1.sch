EESchema Schematic File Version 4
LIBS:main-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L test:LT8697EUDD#PBF U1
U 1 1 60BA5AC6
P 4700 3950
F 0 "U1" H 4700 5117 50  0000 C CNN
F 1 "LT8697EUDD#PBF" H 4700 5026 50  0000 C CNN
F 2 "LT8697EUDD_PBF (1):QFN50P300X500X80-25N" H 4700 3950 50  0001 L BNN
F 3 "" H 4700 3950 50  0001 L BNN
F 4 "IPC 7351B" H 4700 3950 50  0001 L BNN "STANDARD"
F 5 "LINEAR TECHNOLOGY" H 4700 3950 50  0001 L BNN "MANUFACTURER"
F 6 "B" H 4700 3950 50  0001 L BNN "PARTREV"
	1    4700 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 60BA5B5E
P 3250 2950
F 0 "C1" H 3342 2996 50  0000 L CNN
F 1 "4.7uf" H 3342 2905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 3250 2950 50  0001 C CNN
F 3 "~" H 3250 2950 50  0001 C CNN
	1    3250 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery BT1
U 1 1 60BA5BC4
P 2950 2950
F 0 "BT1" H 3058 2996 50  0000 L CNN
F 1 "12V" H 3058 2905 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_3000_1x12mm" V 2950 3010 50  0001 C CNN
F 3 "~" V 2950 3010 50  0001 C CNN
	1    2950 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3150 4000 2750
Wire Wire Line
	4000 2750 3850 2750
Wire Wire Line
	3250 2850 3250 2750
Connection ~ 3250 2750
Wire Wire Line
	3250 2750 2950 2750
Wire Wire Line
	4000 3250 3850 3250
Wire Wire Line
	3850 3250 3850 2750
Connection ~ 3850 2750
Wire Wire Line
	3850 2750 3250 2750
$Comp
L power:GND #PWR0106
U 1 1 60BA5C71
P 2950 3150
F 0 "#PWR0106" H 2950 2900 50  0001 C CNN
F 1 "GND" H 2955 2977 50  0000 C CNN
F 2 "" H 2950 3150 50  0001 C CNN
F 3 "" H 2950 3150 50  0001 C CNN
	1    2950 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 60BA5CA5
P 3250 3050
F 0 "#PWR0107" H 3250 2800 50  0001 C CNN
F 1 "GND" H 3255 2877 50  0000 C CNN
F 2 "" H 3250 3050 50  0001 C CNN
F 3 "" H 3250 3050 50  0001 C CNN
	1    3250 3050
	1    0    0    -1  
$EndComp
NoConn ~ 5400 3250
NoConn ~ 4000 3650
$Comp
L power:GND #PWR0108
U 1 1 60BA5D3D
P 5400 4750
F 0 "#PWR0108" H 5400 4500 50  0001 C CNN
F 1 "GND" V 5405 4622 50  0000 R CNN
F 2 "" H 5400 4750 50  0001 C CNN
F 3 "" H 5400 4750 50  0001 C CNN
	1    5400 4750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 60BA5D74
P 5400 4850
F 0 "#PWR0109" H 5400 4600 50  0001 C CNN
F 1 "GND" V 5405 4722 50  0000 R CNN
F 2 "" H 5400 4850 50  0001 C CNN
F 3 "" H 5400 4850 50  0001 C CNN
	1    5400 4850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 60BA5DCD
P 5550 4550
F 0 "R2" V 5757 4550 50  0000 C CNN
F 1 "18.2k" V 5666 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" V 5480 4550 50  0001 C CNN
F 3 "~" H 5550 4550 50  0001 C CNN
	1    5550 4550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 60BA5EB0
P 5700 4550
F 0 "#PWR0110" H 5700 4300 50  0001 C CNN
F 1 "GND" V 5705 4422 50  0000 R CNN
F 2 "" H 5700 4550 50  0001 C CNN
F 3 "" H 5700 4550 50  0001 C CNN
	1    5700 4550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 60BA60D5
P 3850 4550
F 0 "R1" V 3643 4550 50  0000 C CNN
F 1 "16.5k" V 3734 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" V 3780 4550 50  0001 C CNN
F 3 "~" H 3850 4550 50  0001 C CNN
	1    3850 4550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 60BA6192
P 3700 4550
F 0 "#PWR0111" H 3700 4300 50  0001 C CNN
F 1 "GND" V 3705 4422 50  0000 R CNN
F 2 "" H 3700 4550 50  0001 C CNN
F 3 "" H 3700 4550 50  0001 C CNN
	1    3700 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 60BA620F
P 3900 4450
F 0 "C3" V 3671 4450 50  0000 C CNN
F 1 "2n" V 3762 4450 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 3900 4450 50  0001 C CNN
F 3 "~" H 3900 4450 50  0001 C CNN
	1    3900 4450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 60BA6275
P 3800 4450
F 0 "#PWR0112" H 3800 4200 50  0001 C CNN
F 1 "GND" V 3805 4322 50  0000 R CNN
F 2 "" H 3800 4450 50  0001 C CNN
F 3 "" H 3800 4450 50  0001 C CNN
	1    3800 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 60BA62F2
P 3900 3550
F 0 "C2" V 3671 3550 50  0000 C CNN
F 1 "1u" V 3762 3550 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 3900 3550 50  0001 C CNN
F 3 "~" H 3900 3550 50  0001 C CNN
	1    3900 3550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 60BA6370
P 3800 3550
F 0 "#PWR0113" H 3800 3300 50  0001 C CNN
F 1 "GND" V 3805 3422 50  0000 R CNN
F 2 "" H 3800 3550 50  0001 C CNN
F 3 "" H 3800 3550 50  0001 C CNN
	1    3800 3550
	0    1    1    0   
$EndComp
NoConn ~ 4000 3750
$Comp
L Device:C_Small C6
U 1 1 60BA6402
P 6800 3250
F 0 "C6" H 6892 3296 50  0000 L CNN
F 1 "47u" H 6892 3205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 6800 3250 50  0001 C CNN
F 3 "~" H 6800 3250 50  0001 C CNN
	1    6800 3250
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 60BA64CD
P 6400 3150
F 0 "L1" H 6400 3365 50  0000 C CNN
F 1 "3.3u" H 6400 3274 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 6400 3150 50  0001 C CNN
F 3 "" H 6400 3150 50  0001 C CNN
	1    6400 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60BA65A4
P 7150 3150
F 0 "R4" V 6943 3150 50  0000 C CNN
F 1 "18m" V 7034 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" V 7080 3150 50  0001 C CNN
F 3 "~" H 7150 3150 50  0001 C CNN
	1    7150 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 60BA6621
P 7650 3150
F 0 "R5" V 7443 3150 50  0000 C CNN
F 1 "0.2" V 7534 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" V 7580 3150 50  0001 C CNN
F 3 "~" H 7650 3150 50  0001 C CNN
	1    7650 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 60BA66A7
P 7950 3300
F 0 "R6" H 8020 3346 50  0000 L CNN
F 1 "2.38" H 8020 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" V 7880 3300 50  0001 C CNN
F 3 "~" H 7950 3300 50  0001 C CNN
	1    7950 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 60BA670C
P 7950 3450
F 0 "#PWR0114" H 7950 3200 50  0001 C CNN
F 1 "GND" H 7955 3277 50  0000 C CNN
F 2 "" H 7950 3450 50  0001 C CNN
F 3 "" H 7950 3450 50  0001 C CNN
	1    7950 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 60BA6746
P 6800 3350
F 0 "#PWR0115" H 6800 3100 50  0001 C CNN
F 1 "GND" H 6805 3177 50  0000 C CNN
F 2 "" H 6800 3350 50  0001 C CNN
F 3 "" H 6800 3350 50  0001 C CNN
	1    6800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3350 2700 3350
Wire Wire Line
	2700 3350 2700 2100
Wire Wire Line
	2700 2100 6800 2100
Wire Wire Line
	6800 2100 6800 3150
Wire Wire Line
	6650 3150 6800 3150
Connection ~ 6800 3150
Wire Wire Line
	6800 3150 6900 3150
Wire Wire Line
	7300 3150 7400 3150
Wire Wire Line
	7800 3150 7950 3150
Wire Wire Line
	7950 3150 8350 3150
Connection ~ 7950 3150
Wire Wire Line
	5400 4450 5700 4450
Wire Wire Line
	5700 4450 5700 3550
Wire Wire Line
	5400 3150 5700 3150
Connection ~ 5700 3150
Wire Wire Line
	5700 3150 6150 3150
$Comp
L Device:C_Small C4
U 1 1 60BA6EB1
P 5700 3450
F 0 "C4" H 5792 3496 50  0000 L CNN
F 1 "1u" H 5792 3405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5700 3450 50  0001 C CNN
F 3 "~" H 5700 3450 50  0001 C CNN
	1    5700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3350 5700 3150
Wire Wire Line
	4000 3950 3150 3950
Wire Wire Line
	3150 3950 3150 5250
Wire Wire Line
	3150 5250 6900 5250
Wire Wire Line
	6900 5250 6900 3150
Connection ~ 6900 3150
Wire Wire Line
	6900 3150 7000 3150
Wire Wire Line
	6150 4650 6650 4650
Wire Wire Line
	7400 4650 7400 3950
Connection ~ 7400 3150
Wire Wire Line
	7400 3150 7500 3150
Wire Wire Line
	4000 4050 3400 4050
Wire Wire Line
	3400 4050 3400 5000
Wire Wire Line
	3400 5000 6250 5000
Wire Wire Line
	6250 5000 6250 3950
Wire Wire Line
	6250 3950 6650 3950
Connection ~ 7400 3950
Wire Wire Line
	7400 3950 7400 3150
$Comp
L Device:C_Small C5
U 1 1 60BA7D7F
P 6650 4250
F 0 "C5" H 6742 4296 50  0000 L CNN
F 1 "1n" H 6742 4205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 6650 4250 50  0001 C CNN
F 3 "~" H 6650 4250 50  0001 C CNN
	1    6650 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60BA7DD8
P 7100 4250
F 0 "R3" H 7170 4296 50  0000 L CNN
F 1 "10k" H 7170 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" V 7030 4250 50  0001 C CNN
F 3 "~" H 7100 4250 50  0001 C CNN
	1    7100 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4150 6650 3950
Connection ~ 6650 3950
Wire Wire Line
	6650 3950 7100 3950
Wire Wire Line
	6650 4350 6650 4650
Connection ~ 6650 4650
Wire Wire Line
	6650 4650 7100 4650
Wire Wire Line
	7100 4400 7100 4650
Connection ~ 7100 4650
Wire Wire Line
	7100 4650 7400 4650
Wire Wire Line
	7100 4100 7100 3950
Connection ~ 7100 3950
Wire Wire Line
	7100 3950 7400 3950
Text GLabel 8350 3150 2    50   Input ~ 0
vin(5v)
Wire Wire Line
	5400 3350 5600 3350
Wire Wire Line
	5600 3350 5600 3800
Wire Wire Line
	5600 3800 6150 3800
Wire Wire Line
	6150 3800 6150 4650
Text GLabel 2950 2750 0    50   Input ~ 0
12v
Wire Wire Line
	5400 3150 5400 3050
$EndSCHEMATC
