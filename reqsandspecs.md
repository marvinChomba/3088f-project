## Requirements
User role 1: In the event of power loss, the uHAT should provide ample time for the user to shut down the pi safely. The requirements for this are:
The uHAT must provide power at the correct PI power levels.
The uHAT’s PSU must be rechargeable to allow for reusability
The uHAT must provide power long enough to shutdown the PIsafely.
The uHAT must comply with the standard PiHAT board.
The uHAT should have a back-up ZVD circuit between 5V power pins and the 5V back-powering pins 
 Follows basic HAT GPIO requirements`

## Specifications


Board must have full 40W GPIO connector.
The board must have a minimum of 3 mounting holes with the two adjacent to the GPIO header being a must
The board must be 60×30.5mm
No power supplies are connected to the 3.3V GPIO headers
5V at 1.3A minimum but with the ability to provide 2.5A at 5V







