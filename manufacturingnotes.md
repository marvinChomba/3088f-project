Manufacturing notes:

The manufacturing processe of this PCB should be in 13 simple and easy to follow steps
as listed below :

PCB Manufacturing Process Steps

Step 1: Design and Output

Circuit boards should be compatible with, a PCB layout created using PCB design software. 


Gerber file
These are perfect output format files, they all encode comprehensive vital information including copper tracking layers, drill drawing, apertures, component notations and other options.
All aspects of the PCB design undergo checks at this point thus to be used in manufacture 
A BOM(Bill of Material can be found [here](https://gitlab.com/marvinChomba/3088f-project/-/blob/main/_BOM_.xls) for this PiHAt)



Step 2: From File to Film
Manufacturers must use special printer, which makes photo films of the PCBs, to print circuit boards. 


Step 3: Printing the Inner layers:
This is  Where Will the Copper Go?



Step 4: Drill
using the generated files (drill) files the holes should be drilled in onto the board 
After the drilling completes itself, the additional copper that lines the edges of the production panel undergoes removal by a profiling tool.

Step 5: Plating and Copper Deposition
After drilling, the panel must move onto plating. The process fuses the different layers together using chemical deposition a chemical deposition. The copper goes into the recently drilled holes.



Step 6: Final Etching
The tin protects the desired copper during this stage. The unwanted exposed copper and copper beneath the remaining resist layer undergo removal.


Step 7: Solder Mask Application
The solder mask should be applied to both sides of the board, 
the board should be passes into an oven to cure the solder mask.

Step 8: Surface Finish
To add extra solder-ability to the PCB, one should chemically plate them with gold or silver.

Step 9: Silkscreen
The nearly completed board receives ink-jet writing on its surface,
used to indicate all vital information pertaining to the PCB and components. 

Step 10: Electrical Test
As a final precaution, a technician should performs electrical tests on the PCB. 


Step 11: Profiling and V-Scoring

Now we've come to the last step: cutting. 
Different boards should be cut from the original panel. 

